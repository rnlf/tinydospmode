%macro gdt_entry 4.nolist
  %push gdt
  %define %$base   (%1)
  %define %$limit  (%2)
  %define %$access (%3)
  %define %$flags  (%4)
  dd ((%$limit  & 0x0000FFFF)      ) \
   | ((%$base   & 0x0000FFFF) << 16)
  dd ((%$base   & 0x00FF0000) >> 16) \
   | ((%$base   & 0xFF000000)      ) \
   | ((%$access & 0x000000FF) <<  8) \
   | ((%$flags  & 0x0000000F) << 20) \
   | ((%$limit  & 0x000F0000)      )
  %pop 
%endmacro

%define gdta_PR 0x80
%define gdta_PRIVL(x) (x << 5)
%define gdta_S  0x10
%define gdta_EX 0x08
%define gdta_DC 0x04
%define gdta_RW 0x02
%define gdta_AC 0x01

%define gdtf_GR 0x08
%define gdtf_SZ 0x04

%define gdt_access(h) \
  (gdta_%+h)

%define gdt_access(g,h) \
  (gdta_%+g | gdt_access(h))

%define gdt_access(f,g,h) \
  (gdta_%+f | gdt_access(g,h))

%define gdt_access(e,f,g,h) \
  (gdta_%+e | gdt_access(f,g,h))

%define gdt_access(d,e,f,g,h) \
  (gdta_%+d | gdt_access(e,f,g,h))

%define gdt_access(c,d,e,f,g,h) \
  (gdta_%+c | gdt_access(d,e,f,g,h))

%define gdt_access(b,c,d,e,f,g,h) \
  (gdta_%+b | gdt_access(c,d,e,f,g,h))

%define gdt_access(a,b,c,d,e,f,g,h) \
  (gdta_%+a | gdt_access(b,c,d,e,f,g,h))

%define gdt_flags(b) \
  (gdtf_%+b)

%define gdt_flags(a,b) \
  (gdtf_%+a | gdt_flags(b))

; vim: syn=nasm
