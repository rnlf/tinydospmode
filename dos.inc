%include "psp.inc"

%macro find_argv0 1
    push es
    mov ax, [PSP_EnvironmentSegment]
    mov es, ax
    xor si, si
    mov al, 0xff
  %%next:
    shl ax, 8
    es lodsb
    or ax, ax
    jnz %%next

    add si, 2
    mov [%1], si
    mov [%1+2], es
    
    pop es
%endmacro

; vim: syn=nasm

