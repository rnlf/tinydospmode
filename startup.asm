%include "pmode.inc"
%include "dos.inc"


extern read_buffer
read_buffer_size equ 8*1024
inv_buffer_size equ 256*1024
global read_buffer_size
global inv_buffer_size
%assign required_upper_memory 6*1024
%define use_xms 0
%define use_bios_mem 1

%if use_xms == 1
  %include "xms.inc"
%elif use_bios_mem == 1
  %include "bios.inc"
%endif


  section .stext
  bits 16

extern loader_size
global start
start:
  push cs
  pop ds

  mov ax, cs
  shl ax, 4
  add [payload_entry_dst_far_ptr], ax

  find_argv0 dos_argv0

  ; open self
  mov dx, [dos_argv0]
  push ds
  mov ds, [dos_argv0+2]
  mov ax, 0x3D00
  int 0x21
  pop ds
  jnc .no_open_error

  mov dx, .open_err_msg
  mov ah, 9
  int 0x21
  xor ah, ah
  int 0x21
.open_err_msg: db "Could not open myself for reading. WTF?", 13, 10, "$"
.no_open_error:
  mov [self_file_handle], ax

  ; seek to start of payload
  mov bx, ax
  mov ax, 0x4200
  xor cx, cx
  mov dx, loader_size
  int 0x21

  %if use_xms == 1
    xms_init
  %elif use_bios_mem == 1
    bios_mem_init
  %endif


  pmode_initialize

read_next:
  bits 16
  mov ah, 0x3F
  mov dx, read_buffer
  mov cx, read_buffer_size
  mov bx, [self_file_handle]
  int 0x21
  jnc .no_read_error

  mov dx, .read_err_msg
  mov ah, 9
  int 0x21
  xor ah, ah
  int 0x21
.read_err_msg: db "Could not read myself. WTF?", 13, 10, "$"

.no_read_error:
  or ax, ax
  jz .done_reading

  xor ecx, ecx
  mov cx, ax
  cli
  enter_pmode
  bits 32

  mov ax, dst_data_selector
  mov es, ax
  mov edi, [dst_buf_ptr]
  add dword [dst_buf_ptr], ecx
  mov esi, read_buffer
  rep movsb
  enter_rmode
  sti
  jmp read_next


.done_reading:

  cli
  enter_pmode

  xor esp, esp
  mov sp, [PSP_AllocSize]
  shl esp, 4
  mov ax, dst_data_selector
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  jmp far [cs:payload_entry_dst_far_ptr]

payload_entry:
  mov eax, 0x100000
  call eax

  jmp code_selector:shutdown

shutdown:
  enter_rmode
  sti
  xor ax, ax
  int 0x21


section .data
  payload_entry_dst_far_ptr:
    dd payload_entry
    dw dst_code_selector


  dst_buf_ptr: dd 0x100000



section .bss
  dos_argv0: resw 2
  self_file_handle: resw 2
  read_size: resw 1
  ;a20_test_loc:
;  read_buffer: resb read_buffer_size
;  inv_buffer_overflow: resb (inv_buffer_size - read_buffer_size)


; vim: syn=nasm
