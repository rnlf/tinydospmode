%include "gdt.inc"

extern a20_test_loc

%macro enter_pmode 0
  call do_enter_pmode
  bits 32
%endmacro

%macro enter_rmode 0
  call do_enter_rmode
  bits 16
%endmacro

%macro a20_enable 0
  mov ecx, 1
  call %%a20_test

  ; try BIOS to activate
  mov ax, 0x2401
  int 0x15

  mov ecx, 1
  call %%a20_test

  ; try keyboard controller
  cli
  call %%kbdwait
  mov al, 0xAD
  out 0x64, al

  call %%kbdwait
  mov al, 0xD0
  out 0x64, al

  call %%kbdwait2
  in al, 0x60
  push eax

  call %%kbdwait
  mov al, 0xD1
  out 0x64, al

  call %%kbdwait
  pop eax
  or al, 2
  out 0x60, al

  call %%kbdwait
  mov al, 0xAE
  out 0x64, al

  call %%kbdwait
  sti

  mov ecx, 100
  call %%a20_test

  ; try Fast A20 Gate
  in al, 0x92
  test al, 2
  jnz %%dont_fga20
  or al, 2
  and al, 0xFE
  out 0x92, al
%%dont_fga20:
  
  mov ecx, 100
  call %%a20_test

  mov dx, %%a20_failure
  mov ah, 9
  int 0x21
  xor ah, ah
  int 0x21
%%a20_failure: db "Could not activate A20. WTF?", 13, 10, "$"

%%a20_test:
  cli
  enter_pmode
%%retry:
  mov edi, a20_test_loc + 0x100000
  mov [edi], edi

  ; try hard to invalidate cache
  push ecx
  push edi
  mov ecx, a20_test_size / 16 - 1
  xor eax, eax
%%next_inv_cache:
  add edi, 16
  mov [edi], eax
  loop %%next_inv_cache

  mov esi, a20_test_loc
  mov [esi], esi

  pop edi
  pop ecx
  cmpsd
  jne %%done
  loop %%retry

  pop bx
  enter_rmode
  sti
  jmp bx
  

%%kbdwait:
  in      al,0x64
  test    al,2
  jnz     %%kbdwait
  ret

%%kbdwait2:
  in      al,0x64
  test    al,1
  jz      %%kbdwait2
  ret

%%done:
  bits 32
  enter_rmode
  add sp, 2
  sti

%endmacro


%macro pmode_initialize 0
  ; fix up gtdt according to actual CS
  mov eax, cs
  shl eax, 4
  add [gdtd+2], eax

  ; fix up loader code and data segment descriptors
  ; so that absolute addresses remain valid after
  ; switching to pmode
  mov [code_segment_descriptor+2], ax
  mov [data_segment_descriptor+2], ax

  shr eax, 16
  mov [code_segment_descriptor+4], al
  mov [data_segment_descriptor+4], al
  mov [code_segment_descriptor+7], ah
  mov [data_segment_descriptor+7], ah

  ; fix pmode stack segment selector so (e)sp remains
  ; valid after switching
; xor eax, eax
; mov ax, ss
; shl eax, 4
; mov [stack_segment_descriptor+2], ax
; shr eax, 16
; mov [stack_segment_descriptor+4], al
; mov [stack_segment_descriptor+7], ah

  mov [rmode_continue_segment], cs

  ;enable_a20_gate

  lgdt [gdtd]

  a20_enable
%endmacro


  section .text

do_enter_pmode:
  bits 16 
  mov eax, cr0
  or eax, 1
  mov cr0, eax
  jmp word code_selector:pmode_enter_continue

pmode_enter_continue:
  bits 32
  mov ax, data_selector
  mov ds, ax
  mov es, ax
  ;mov ax, stack_selector
  mov ss, ax
  ; call in pmode pushed eip, ret in rmode would pop ip
  ; without this, we leak two bytes on the stack
  xor eax, eax
  pop ax
  jmp eax



do_enter_rmode:
  mov eax, cr0
  and eax, ~1
  mov cr0, eax
  jmp far dword [cs:rmode_continue_farptr]

rmode_enter_continue:
  bits 16
  mov ax, cs
  mov ds, ax
  mov ss, ax
  ; call in pmode pushed eip, ret in rmode would pop ip
  ; without this, we leak two bytes on the stack
  pop eax
  jmp ax




section .data
  rmode_continue_farptr:  dd rmode_enter_continue
  rmode_continue_segment: dw 0

global gdt
gdt:
gdtd:
  ; null descriptor
  dw (gdt_end-gdt)
  dd gdt
  dw 0
  code_selector equ $ - gdt
  code_segment_descriptor:
  gdt_entry 0, 0xFFFFF,                      \
    gdt_access(PR, PRIVL(0), S, EX, RW),     \
    gdt_flags(GR, SZ)

  data_selector equ $ - gdt
  data_segment_descriptor:
  gdt_entry 0, 0xFFFFF,                      \
    gdt_access(PR, PRIVL(0), S, RW),         \
    gdt_flags(GR, SZ)

; stack_selector equ $ - gdt
; stack_segment_descriptor:
; gdt_entry 0, 0xFFFFF,                      \
;   gdt_access(PR, PRIVL(0), S, RW),         \
;   gdt_flags(GR, SZ)


  dst_code_selector equ $ - gdt
  dst_code_segment_descriptor:
  gdt_entry 0, 0xFFFFF,                      \
    gdt_access(PR, PRIVL(0), S, EX, RW),     \
    gdt_flags(GR, SZ)

  dst_data_selector equ $ - gdt
  dst_data_segment_descriptor:
  gdt_entry 0, 0xFFFFF,                      \
    gdt_access(PR, PRIVL(0), S, RW),         \
    gdt_flags(GR, SZ)
gdt_end:


section .bss

a20_test_size equ inv_buffer_size
; vim: syn=nasm
