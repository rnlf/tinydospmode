#!/bin/bash -e

nasm -felf32 -o startup.o startup.asm
i686-linux-gnu-ld -M -T link.ld -o startup.exe startup.o


gcc-7 -nostdinc -m32 -ffreestanding -nostdlib -nodefaultlibs -fno-pie -fomit-frame-pointer -fno-asynchronous-unwind-tables -c -O3 -o test.o test.c
i686-linux-gnu-ld -M -T test.ld -o test.bin test.o

cat startup.exe test.bin > startup2.exe
