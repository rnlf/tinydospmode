%macro xms_init 0
  ; test XMS availability
  bits 16
  mov ax,0x4300
  int 0x2f
  cmp al, 0x80
  jne no_xms_driver

  ; get driver control function
  mov ax, 0x4310
  int 0x2f
  mov [xms_control_func], bx
  mov [xms_control_func+2], es

  ; check minimum XMS version
  xor ah, ah
  call far [xms_control_func]
  cmp ax, 0x0200
  jb  no_xms_driver

  ; enable a20 gate
  mov ah, 3
  call far [xms_control_func]
  cmp ax, 1
  jne no_a20

  ; request largest available extended memory block
  mov ah, 8
  call far [xms_control_func]
  or bl, bl
  jnz generic_xms_error
  mov dx, ax

  mov ah, 9
  call far [xms_control_func]
  cmp ax, 1
  jne no_mem
  mov [xms_block_handle], dx

  ; lock allocated memory
  mov ah, 12
  call far [xms_control_func]
  cmp ax, 1
  jne no_lock
%endmacro


  section .bss

xms_control_func: resw 2
xms_block_handle: resw 1

  bits 16
  section .text

no_xms_driver:
  mov dx, no_xms_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21

no_a20:
  mov dx, no_a20_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21

no_mem:
  mov dx, no_mem_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21

no_lock:
  mov dx, no_lock_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21

generic_xms_error:
  mov dx, generic_error_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21


section .data

no_xms_msg:
  db "XMS 2.0 or newer required but not found.", 13, 10, "$"
no_a20_msg:
  db "Could not enable A20.", 13, 10, "$"
no_mem_msg:
  db "Not enough memory.", 13, 10, "$"
no_lock_msg:
  db "Could not lock extended memory block.", 13, 10, "$"
generic_error_msg:
  db "Unknown error in XMS.", 13, 10, "$"

; vim: syn=nasm
