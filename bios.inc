%ifndef required_upper_memory
  %error "please define required_upper_memory before including bios.inc"
%endif

%macro bios_mem_init 0
  mov ah, 0x88
  int 0x15
  or ax, ax
  jz no_high_mem
  cmp ax, required_upper_memory
  jb not_enough_memory
%endmacro


  bits 16
  section .text

not_enough_memory:
  mov dx, not_enough_memory_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21

no_high_mem:
  mov dx, no_high_mem_msg
  mov ah, 9
  int 0x21

  xor ah, ah
  int 0x21


  section .text

%defstr req_mem required_upper_memory


no_high_mem_msg:
  db "No high memory detected. Please disable XMS and EMS.", 13, 10, "$"

not_enough_memory_msg:
  db "Not enough upper memory: ", req_mem, "K required.", 13, 10, "$"

; vim: syn=nasm
